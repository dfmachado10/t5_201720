package model.dataestructures.test;

import java.util.Random;

import javax.swing.text.html.HTMLDocument.Iterator;

import junit.framework.TestCase;
import model.data_structures.DoubleLinkedList;
import model.data_structures.DoubleLinkedList.MiIterator;

public class DoubleLinkedListTest extends TestCase{
	
	private DoubleLinkedList<Integer> listaInt;
	private DoubleLinkedList<String> listaString;
	private long sizeAlt;
	
	private void setupEscenario1(){
		listaInt = new DoubleLinkedList<Integer>();
		listaString = new DoubleLinkedList<String>();
	}
	
	private void setupEscenario2(){
		listaInt = new DoubleLinkedList<Integer>();
		Random r = new Random();
		for (int i = 0; i < 3; i++) {
			listaInt.addAtEnd(Integer.valueOf(r.nextInt(30)));
		}
		
		listaString = new DoubleLinkedList<String>();
		for (int i = 0; i < 3; i++) {
			char c = (char)(r.nextInt(26) + 'a');
			listaString.addAtEnd("" + c);
		}
	}
	
	private void setupEscenario3(){
		listaInt = new DoubleLinkedList<Integer>();
		Random r = new Random();
		sizeAlt = r.nextInt(100);
		if(sizeAlt<0) sizeAlt = sizeAlt* (-1);
		for (int i = 0; i < sizeAlt; i++) {
			listaInt.addAtEnd(Integer.valueOf(r.nextInt(50)));
		}
		
		listaString = new DoubleLinkedList<String>();
		for (int i = 0; i < sizeAlt; i++) {
			char c = (char)(r.nextInt(26) + 'a');
			listaString.addAtEnd("" + c);
		}
	}
	
	public void testDoubleLinkedList(){
		setupEscenario1();
		assertNotNull("Las listas debieron inicializarse", listaInt);
		assertNotNull("Las listas debieron inicializarse", listaString);
		assertNull("Debe inicializar la primera como null", listaInt.firstOne());
		assertNull("La lsita debio iniciar la ultima como null", listaInt.lastOne());
		assertNull("Debe inicializar el actual como null", listaInt.getCurrentElement());
		assertEquals("El tama�o de la lista debe ser 0 desde el principio", 0, listaInt.getSize());
		assertNull("Debe inicializar la primera como null", listaString.firstOne());
		assertNull("La lsita debio iniciar la ultima como null", listaString.lastOne());
		assertNull("Debe inicializar el actual como null", listaString.getCurrentElement());
		assertEquals("El tama�o de la lista debe ser 0 desde el principio", 0, listaString.getSize());
	}
	
	public void testAdd1(){
		setupEscenario1();
		
		boolean agregadaInt = listaInt.add(Integer.valueOf(1));
		boolean agregadaStn = listaString.add("�");
		assertTrue("Se debe poder agregar el primer elemento", agregadaInt);
		assertTrue("Se debe poder agregar el primer elemento", agregadaStn);
		
		assertEquals("El primer objeto debe ser el primer elemento agregado a la lista", Integer.valueOf(1), listaInt.firstOne());
		assertEquals("El primer objeto debe ser el primer elemento agregado a la lista", "�", listaString.firstOne());
		assertEquals("El ultimo objeto debe ser el primer elemento agregado a la lista", Integer.valueOf(1), listaInt.lastOne());
		assertEquals("El ultimo objeto debe ser el primer elemento agregado a la lista", "�", listaString.lastOne());
		assertEquals("El objeto actual debe ser el primer elemento agregado a la lista", Integer.valueOf(1), listaInt.getCurrentElement());
		assertEquals("El objeto actual debe ser el primer elemento agregado a la lista", "�", listaString.getCurrentElement());
		assertEquals("El arreglo debe tener un tama�o de 1", 1, listaInt.getSize());
		assertEquals("El arreglo debe tener un tama�o de 1", 1, listaString.getSize());
		assertEquals("El objeto en la primera posicion debe ser el primer elemento agregado a la lista", Integer.valueOf(1), listaInt.getElement(0));
		assertEquals("El objeto en la primera posicion debe ser el primer elemento agregado a la lista", "�", listaString.getElement(0));
	}
	
	// TODO
	public void testAdd2(){
		setupEscenario2();
		
		boolean agregadaInt = listaInt.add(Integer.valueOf(1));
		boolean agregadaStn = listaString.add("�");
		assertTrue("Se debe poder agregar el primer elemento", agregadaInt);
		assertTrue("Se debe poder agregar el primer elemento", agregadaStn);
		
		assertEquals("El primer objeto debe ser este nuevo objeto agregado", Integer.valueOf(1), listaInt.firstOne());
		assertEquals("El primer objeto debe ser este nuevo objeto agregado", "�", listaString.firstOne());
		assertEquals("El arreglo debe tener un tama�o de 4", 4, listaInt.getSize());
		assertEquals("El arreglo debe tener un tama�o de 4", 4, listaString.getSize());
		assertEquals("El objeto en la primera posicion debe ser este nuevo objeto agregado", Integer.valueOf(1), listaInt.getElement(0));
		assertEquals("El objeto en la primera posicion debe ser este nuevo objeto agregado", "�", listaString.getElement(0));
	}
	
	public void testAdd3(){
		setupEscenario3();
		
		boolean agregadaInt = listaInt.add(Integer.valueOf(1));
		boolean agregadaStn = listaString.add("�");
		assertTrue("Se debe poder agregar el primer elemento", agregadaInt);
		assertTrue("Se debe poder agregar el primer elemento", agregadaStn);
		
		assertEquals("El primer objeto debe ser este nuevo objeto agregado", Integer.valueOf(1), listaInt.firstOne());
		assertEquals("El primer objeto debe ser este nuevo objeto agregado", "�", listaString.firstOne());
		assertEquals("El arreglo debe tener un tama�o de " + (sizeAlt+1), sizeAlt+1, listaInt.getSize());
		assertEquals("El arreglo debe tener un tama�o de " + (sizeAlt+1), sizeAlt+1, listaString.getSize());
		assertEquals("El objeto en la primera posicion debe ser este nuevo objeto agregado", Integer.valueOf(1), listaInt.getElement(0));
		assertEquals("El objeto en la primera posicion debe ser este nuevo objeto agregado", "�", listaString.getElement(0));
	}
	
	public void testAddAtEnd1(){
		setupEscenario1();
		
		boolean agregadaInt = listaInt.addAtEnd(Integer.valueOf(1));
		boolean agregadaStn = listaString.addAtEnd("�");
		assertTrue("Se debe poder agregar el primer elemento", agregadaInt);
		assertTrue("Se debe poder agregar el primer elemento", agregadaStn);
		
		assertEquals("El primer objeto debe ser el primer elemento agregado a la lista", Integer.valueOf(1), listaInt.firstOne());
		assertEquals("El primer objeto debe ser el primer elemento agregado a la lista", "�", listaString.firstOne());
		assertEquals("El ultimo objeto debe ser el primer elemento agregado a la lista", Integer.valueOf(1), listaInt.lastOne());
		assertEquals("El ultimo objeto debe ser el primer elemento agregado a la lista", "�", listaString.lastOne());
		assertEquals("El objeto actual debe ser el primer elemento agregado a la lista", Integer.valueOf(1), listaInt.getCurrentElement());
		assertEquals("El objeto actual debe ser el primer elemento agregado a la lista", "�", listaString.getCurrentElement());
		assertEquals("El arreglo debe tener un tama�o de 1", 1, listaInt.getSize());
		assertEquals("El arreglo debe tener un tama�o de 1", 1, listaString.getSize());
		assertEquals("El objeto en la primera posicion debe ser el primer elemento agregado a la lista", Integer.valueOf(1), listaInt.getElement(0));
		assertEquals("El objeto en la primera posicion debe ser el primer elemento agregado a la lista", "�", listaString.getElement(0));
	}
	
	public void testAddAtEnd2(){
		setupEscenario2();
		
		boolean agregadaInt = listaInt.addAtEnd(Integer.valueOf(1));
		boolean agregadaStn = listaString.addAtEnd("�");
		assertTrue("Se debe poder agregar el primer elemento", agregadaInt);
		assertTrue("Se debe poder agregar el primer elemento", agregadaStn);
		
		assertEquals("El ultimo objeto debe ser el primer elemento agregado a la lista", Integer.valueOf(1), listaInt.lastOne());
		assertEquals("El ultimo objeto debe ser el primer elemento agregado a la lista", "�", listaString.lastOne());
		assertEquals("El arreglo debe tener un tama�o de 4", 4, listaInt.getSize());
		assertEquals("El arreglo debe tener un tama�o de 4", 4, listaString.getSize());
		assertEquals("El objeto en la ultima posicion debe ser este nuevo objeto agregado", Integer.valueOf(1), listaInt.getElement(3));
		assertEquals("El objeto en la ultima posicion debe ser este nuevo objeto agregado", "�", listaString.getElement(3));
	}
	
	public void testAddAtEnd3(){
		setupEscenario3();
		
		boolean agregadaInt = listaInt.addAtEnd(Integer.valueOf(1));
		boolean agregadaStn = listaString.addAtEnd("�");
		assertTrue("Se debe poder agregar el primer elemento", agregadaInt);
		assertTrue("Se debe poder agregar el primer elemento", agregadaStn);
		
		assertEquals("El ultimo objeto debe ser este nuevo objeto agregado", Integer.valueOf(1), listaInt.lastOne());
		assertEquals("El ultimo objeto debe ser este nuevo objeto agregado", "�", listaString.lastOne());
		assertEquals("El arreglo debe tener un tama�o de " + (sizeAlt+1), sizeAlt+1, listaInt.getSize());
		assertEquals("El arreglo debe tener un tama�o de " + (sizeAlt+1), sizeAlt+1, listaString.getSize());
		assertEquals("El objeto en la ultima posicion debe ser este nuevo objeto agregado", Integer.valueOf(1), listaInt.getElement((int)sizeAlt));
		assertEquals("El objeto en la ultima posicion debe ser este nuevo objeto agregado", "�", listaString.getElement((int)sizeAlt));
	}
	
	public void testAddAtK1(){
		setupEscenario1();
		
		boolean agregadaInt = listaInt.addAtK(Integer.valueOf(1), 0);
		boolean agregadaStn = listaString.addAtK("�", 0);
		assertTrue("Se debe poder agregar el primer elemento", agregadaInt);
		assertTrue("Se debe poder agregar el primer elemento", agregadaStn);
		
		assertEquals("El primer objeto debe ser el primer elemento agregado a la lista", Integer.valueOf(1), listaInt.firstOne());
		assertEquals("El primer objeto debe ser el primer elemento agregado a la lista", "�", listaString.firstOne());
		assertEquals("El ultimo objeto debe ser el primer elemento agregado a la lista", Integer.valueOf(1), listaInt.lastOne());
		assertEquals("El ultimo objeto debe ser el primer elemento agregado a la lista", "�", listaString.lastOne());
		assertEquals("El objeto actual debe ser el primer elemento agregado a la lista", Integer.valueOf(1), listaInt.getCurrentElement());
		assertEquals("El objeto actual debe ser el primer elemento agregado a la lista", "�", listaString.getCurrentElement());
		assertEquals("El arreglo debe tener un tama�o de 1", 1, listaInt.getSize());
		assertEquals("El arreglo debe tener un tama�o de 1", 1, listaString.getSize());
		assertEquals("El objeto en la primera posicion debe ser el primer elemento agregado a la lista", Integer.valueOf(1), listaInt.getElement(0));
		assertEquals("El objeto en la primera posicion debe ser el primer elemento agregado a la lista", "�", listaString.getElement(0));
	}
	
	public void testAddAtK2(){
		setupEscenario2();
		
		boolean agregadaInt = listaInt.addAtK(Integer.valueOf(1), 3);
		boolean agregadaStn = listaString.addAtK("�", 3);
		assertTrue("Se debe poder agregar este nuevo elemento", agregadaInt);
		assertTrue("Se debe poder agregar este nuevo elemento", agregadaStn);
		
		assertEquals("El ultimo objeto debe ser este nuevo objeto agregado", Integer.valueOf(1), listaInt.lastOne());
		assertEquals("El ultimo objeto debe ser este nuevo objeto agregado", "�", listaString.lastOne());
		assertEquals("El arreglo debe tener un tama�o de 4", 4, listaInt.getSize());
		assertEquals("El arreglo debe tener un tama�o de 4", 4, listaString.getSize());
		assertEquals("El objeto en la ultima posicion debe ser este nuevo objeto agregado", Integer.valueOf(1), listaInt.getElement(3));
		assertEquals("El objeto en la ultima posicion debe ser este nuevo objeto agregado", "�", listaString.getElement(3));
	}
	
	public void testAddAtK3(){
		setupEscenario3();
		
		Random r = new Random();
		int k = r.nextInt((int)sizeAlt/2);
		if(k<0) k=((-1)*k);
		boolean agregadaInt = listaInt.addAtK(Integer.valueOf(1), k);
		boolean agregadaStn = listaString.addAtK("�", k);
		assertTrue("Se debe poder agregar el primer elemento", agregadaInt);
		assertTrue("Se debe poder agregar el primer elemento", agregadaStn);
		
		assertEquals("El arreglo debe tener un tama�o de " + (sizeAlt+1), (sizeAlt+1), listaInt.getSize());
		assertEquals("El arreglo debe tener un tama�o de " + (sizeAlt+1), (sizeAlt+1), listaString.getSize());
		assertEquals("El objeto en la ultima posicion debe ser este nuevo objeto agregado", Integer.valueOf(1), listaInt.getElement(k));
		assertEquals("El objeto en la ultima posicion debe ser este nuevo objeto agregado", "�", listaString.getElement(k));
	}
	
	public void testAddAtKError(){
		setupEscenario3();
		
		boolean agregadaInt = listaInt.addAtK(Integer.valueOf(1), -1);
		boolean agregadaStn = listaString.addAtK("�", -1);
		
		assertFalse("El elemento no se debio agregar en una posicion no existente", agregadaInt);
		assertFalse("El elemento no se debio agregar en una posicion no existente", agregadaStn);
		assertEquals("El tama�o del la lista no debio cambiar", sizeAlt, listaInt.getSize());
		assertEquals("El tama�o del la lista no debio cambiar", sizeAlt, listaString.getSize());
	}
	
	public void testGetElement1(){
		setupEscenario1();
		
		listaInt.add(Integer.valueOf(1));
		listaString.add("�");
		assertEquals("El elemento que se quiere conseguir no coincide", Integer.valueOf(1), listaInt.getElement(0));
		assertEquals("El elemento que se quiere conseguir no coincide", "�", listaString.getElement(0));
	}
	
	public void testGetElement2(){
		setupEscenario2();
		
		listaInt.addAtEnd(Integer.valueOf(1));
		listaString.addAtEnd("�");
		assertEquals("El elemento que se quiere conseguir no coincide", Integer.valueOf(1), listaInt.getElement(3));
		assertEquals("El elemento que se quiere conseguir no coincide", "�", listaString.getElement(3));
	}
	
	public void testGetElement3(){
		setupEscenario3();
		
		Random r = new Random();
		int k = r.nextInt((int)sizeAlt/2);
		if(k<0) k=((-1)*k);
		listaInt.addAtK(Integer.valueOf(1), k);
		listaString.addAtK("�", k);
		assertEquals("El elemento que se quiere conseguir no coincide", Integer.valueOf(1), listaInt.getElement(k));
		assertEquals("El elemento que se quiere conseguir no coincide", "�", listaString.getElement(k));
	}
	
	public void testeGetElementError(){
		setupEscenario3();
		
		int a = 0, b = 0;
		Object resp = null; 
		
		try {
			listaInt.getElement(-1);
		}
		catch(IndexOutOfBoundsException e){
			a++;
		}
		try {
			listaString.getElement(-1);
		} 
		catch (IndexOutOfBoundsException e) {
			b++;
		}
		
		if(a == 0 || b == 0){
			fail("La lista no esta enviando las excepciones esperadas");
		}
	}
	
	public void testGetCurrentElement1(){
		setupEscenario1();
		
		listaInt.add(Integer.valueOf(1));
		listaString.add("�");
		assertEquals("El elemento que se quiere conseguir no coincide", Integer.valueOf(1), listaInt.getCurrentElement());
		assertEquals("El elemento que se quiere conseguir no coincide", "�", listaString.getCurrentElement());
	}
	
	public void testGetCurrentElement2(){
		setupEscenario2();
		
		listaInt.addAtEnd(Integer.valueOf(1));
		for (int i = 0; i < 4; i++) listaInt.next();
		listaString.addAtEnd("�");
		for (int i = 0; i < 4; i++) listaString.next();
		
		assertEquals("El elemento que se quiere conseguir no coincide", Integer.valueOf(1), listaInt.getCurrentElement());
		assertEquals("El elemento que se quiere conseguir no coincide", "�", listaString.getCurrentElement());
	}
	
	public void testGetCurrentElement3(){
		setupEscenario3();
		
		Random r = new Random();
		int k = r.nextInt((int)sizeAlt/2);
		if(k<0) k=((-1)*k);
		listaInt.addAtK(Integer.valueOf(1), k);
		for (int i = 0; i < k; i++) listaInt.next();
		listaString.addAtK("�", k);
		for (int i = 0; i < k; i++) listaString.next();
		
		assertEquals("El elemento que se quiere conseguir no coincide", Integer.valueOf(1), listaInt.getCurrentElement());
		assertEquals("El elemento que se quiere conseguir no coincide", "�", listaString.getCurrentElement());
	}
	
	public void testNext(){
		setupEscenario1();
		
		listaInt.add(Integer.valueOf(1));
		listaInt.next();
		listaString.add("�");
		listaString.next();
		assertEquals("El elemento que se quiere conseguir no debe cambiar al aplicar un next", Integer.valueOf(1), listaInt.getCurrentElement());
		assertEquals("El elemento que se quiere conseguir no debe cambiar al aplicar un next", "�", listaString.getCurrentElement());
		
		listaInt.addAtEnd(Integer.valueOf(2));
		listaInt.next();
		listaString.addAtEnd("b");
		listaString.next();
		assertEquals("El elemento que se quiere conseguir debio cambiar al aplicar el next", Integer.valueOf(2), listaInt.getCurrentElement());
		assertEquals("El elemento que se quiere conseguir debio cambiar al aplicar el next", "b", listaString.getCurrentElement());
	}
	
	public void testPrevious(){
		setupEscenario1();
		
		listaInt.add(Integer.valueOf(1));
		listaInt.previous();
		listaString.add("�");
		listaString.previous();
		assertEquals("El elemento que se quiere conseguir no debe cambiar al aplicar un previous", Integer.valueOf(1), listaInt.getCurrentElement());
		assertEquals("El elemento que se quiere conseguir no debe cambiar al aplicar un previous", "�", listaString.getCurrentElement());
		
		listaInt.add(Integer.valueOf(2));
		listaInt.previous();
		listaString.add("b");
		listaString.previous();
		assertEquals("El elemento que se quiere conseguir debio cambiar al aplicar el previous", Integer.valueOf(2), listaInt.getCurrentElement());
		assertEquals("El elemento que se quiere conseguir debio cambiar al aplicar el previous", "b", listaString.getCurrentElement());
	}
	
	public void testGetSize(){
		setupEscenario1();
		
		assertEquals("El arreglo no debe aumentar de tama�o si el arreglo no tiene nada", 0, listaInt.getSize());
		assertEquals("El arreglo no debe aumentar de tama�o si el arreglo no tiene nada", 0, listaString.getSize());
		
		listaInt.add(Integer.valueOf(1));
		listaString.add("�");
		assertEquals("El arreglo debio aumentar el tama�o al agregar un elemento a la lista", 1, listaInt.getSize());
		assertEquals("El arreglo debio aumentar el tama�o al agregar un elemento a la lista", 1, listaString.getSize());
		
		listaInt.deleteAtK(0);
		listaString.deleteAtK(0);
		assertEquals("El arreglo debe disminuir el tama�o al borrar un objeto", 0, listaInt.getSize());
		assertEquals("El arreglo debe disminuir el tama�o al borrar un objeto", 0, listaString.getSize());
	}
	
	public void testDelete(){
		setupEscenario1();
		
		listaInt.add(Integer.valueOf(1));
		listaString.add("�");
		assertEquals("El elemento eliminado debe ser el pedido", Integer.valueOf(1), listaInt.delete(Integer.valueOf(1)));
		assertEquals("El elemento eliminado debe ser el que fue pedido", "�", listaString.delete("�"));
		assertEquals("El tama�o del arreglo debe de disminuir al eliminar un elemento", 0, listaInt.getSize());
		assertEquals("El tama�o del arreglo debe de disminuir al eliminar un elemento", 0, listaString.getSize());
	}
	
	public void testDeleteError(){
		setupEscenario1();
		
		int a = 0, b = 0;
		Object resp = null; 
		
		try {
			listaInt.delete(Integer.valueOf(1));
		}
		catch(IndexOutOfBoundsException e){
			a++;
		}
		try {
			listaString.delete("�");
		} 
		catch (IndexOutOfBoundsException e) {
			b++;
		}
		
		if(a == 0 || b == 0){
			fail("La lista no esta enviando las excepciones esperadas");
		}
	}
	
	public void testDeleteAtK1(){
		setupEscenario1();
		
		listaInt.add(Integer.valueOf(1)); listaString.add("�");
		Integer elim1 = listaInt.deleteAtK(0); String elim2 = listaString.deleteAtK(0);
		assertEquals("La lista no esta eliminando el elemento", Integer.valueOf(1), elim1);
		assertEquals("La lista no esta eliminando el elemento", "�", elim2);
		assertEquals("La lista no esta disminuyendo el tama�o al eliminar un objeto", 0, listaInt.getSize());
		assertEquals("La lista no esta disminuyendo el tama�o al eliminar un objeto", 0, listaString.getSize());
	}
	
	public void testDeleteAtK2(){
		setupEscenario2();
		
		listaInt.addAtEnd(Integer.valueOf(1)); listaString.addAtEnd("�");
		Integer elim1 = listaInt.deleteAtK(listaInt.getSize()-1); String elim2 = listaString.deleteAtK(listaString.getSize()-1);
		assertEquals("La lista no esta eliminando el elemento", Integer.valueOf(1), elim1);
		assertEquals("La lista no esta eliminando el elemento", "�", elim2);
		assertEquals("La lista no esta disminuyendo el tama�o al eliminar un objeto", 3, listaInt.getSize());
		assertEquals("La lista no esta disminuyendo el tama�o al eliminar un objeto", 3, listaString.getSize());
	}
	
	public void testDeleteAtK3(){
		setupEscenario3();
		
		Random r = new Random();
		int k = r.nextInt((int)sizeAlt/2);
		if(k<0) k=((-1)*k);
		listaInt.addAtK(Integer.valueOf(1), k);
		listaString.addAtK("�", k);
		
		Integer elim1 = listaInt.deleteAtK(k); String elim2 = listaString.deleteAtK(k);
		assertEquals("La lista no esta eliminando el elemento", Integer.valueOf(1), elim1);
		assertEquals("La lista no esta eliminando el elemento", "�", elim2);
		assertEquals("La lista no esta disminuyendo el tama�o al eliminar un objeto", sizeAlt, listaInt.getSize());
		assertEquals("La lista no esta disminuyendo el tama�o al eliminar un objeto", sizeAlt, listaString.getSize());
	}
	
	public void testDeleteAtKError(){
		setupEscenario1();
		
		int a = 0, b = 0;
		Object resp = null; 
		
		try {
			listaInt.deleteAtK(-1);
		}
		catch(IndexOutOfBoundsException e){
			a++;
		}
		try {
			listaString.deleteAtK(-1);
		} 
		catch (IndexOutOfBoundsException e) {
			b++;
		}
		
		if(a == 0 || b == 0) fail("La lista no esta enviando las excepciones esperadas");
	}
	
	public void testIterator(){
		setupEscenario3();
		
		java.util.Iterator<Integer> it1 = listaInt.iterator();
		java.util.Iterator<String> it2 = listaString.iterator();
		
		int i = 0, correcto1 = 0;
		Integer a = it1.next();
		while(it1.hasNext()){
			if(listaInt.getElement(i) == a) correcto1++;
			i++; a = it1.next();
		}
		
		int k = 0, correcto2 = 0;
		String s = it2.next();
		while(it2.hasNext()){
			if(listaString.getElement(k) == s) correcto2++;
			k++; s = it2.next();
		}
		
		if(correcto1 != (int)(sizeAlt-1) || correcto2 != (int)(sizeAlt-1)) fail("El iterator no esta funcionando correctamente");
	}
}
