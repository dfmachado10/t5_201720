package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Iterator;
import java.util.Stack;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;
import model.data_structures.IQueue;
import model.data_structures.IStacks;
import model.data_structures.LinkedList;
import model.data_structures.MyMergeSort;
import model.data_structures.Queue;
import model.data_structures.Stacks;
import model.vo.VOViaje;
import api.ISTSManager;

public class STSManager implements ISTSManager{



	private IList<VOViaje> trips;


	public void loadTrips() {
		// TODO Auto-generated method stub
		trips =  new DoubleLinkedList<VOViaje>();

		try
		{
			BufferedReader bf = new BufferedReader( new FileReader( "data/trips.txt" ) );
			String line = bf.readLine( );
			line=bf.readLine();

			while(line!=null)
			{
				String[] parts= line.split(",");
				int route_id=Integer.parseInt(parts[0].trim());
				int service_id=Integer.parseInt(parts[1].trim());
				int trip_id=Integer.parseInt(parts[2].trim());
				String trip_headsign=parts[3];
				String trip_short_name=parts[4].trim();
				int direction_id=Integer.parseInt(parts[4].trim());
				int block_id=Integer.parseInt(parts[6].trim());
				String shape_id=parts[7].trim();
				boolean wheelchair_accessible=false;
				if(Integer.parseInt(parts[8].trim())==1) wheelchair_accessible=true;
				boolean bikes_allowed=false;
				if(Integer.parseInt(parts[9].trim())==1) bikes_allowed=true;

				VOViaje nuevo= new VOViaje(route_id, trip_id, service_id, trip_headsign, trip_short_name,
						direction_id, block_id, shape_id, wheelchair_accessible, bikes_allowed);

				trips.add(nuevo);

				line=bf.readLine();
			}
			bf.close();


		} catch (Exception e) {
			// TODO: handle exception
		}


	}

	public IList<VOViaje> sortByBlockId(){
		IList<VOViaje> res= new DoubleLinkedList<VOViaje>();
		int[] arr= new int[(int) trips.getSize()];
		for (int i = 0; i < trips.getSize(); i++) {
			VOViaje indx= trips.getElement(i);
			arr[i]=indx.getBlockId();		
		}
		MyMergeSort merge= new MyMergeSort();
		merge.sort(arr);
		for (int i = 0; i < arr.length; i++) {
			int blockIndx=arr[i];			
			for (int j = 0; j < trips.getSize(); j++) {
				VOViaje indx= trips.getElement(j);
				if(indx.getBlockId()==blockIndx) res.add(indx);			
			}
		}	
		return res;
	}

	public int blockIdRepetidos(int pBlock, IList<VOViaje> pViajes){
		int respuesta=0;
		for (int i = 0; i < pViajes.getSize() ; i++) {
			if(pBlock == pViajes.getElement(i).getBlockId())
				respuesta++;	
		}
		return respuesta;
	}

	public int[] tripIdAsociados(int pBlock, IList<VOViaje> pViajes){
		int c=0;
		int[] asociados=new int[c];
		for (int i = 0; i < pViajes.getSize() ; i++) {
			VOViaje indx= pViajes.getElement(i);
			if(pBlock == indx.getBlockId()){
				c++;
				asociados[i]=indx.getIdViaje();
			}

		}
		return asociados;
	}

	public IList<Resumen> darResumenList(){
		IList<Resumen> resumenes=new LinkedList<Resumen>();
		IList<VOViaje> ordenados= sortByBlockId();
		int blockIdActual=0;
		for (int i = 0; i < ordenados.getSize(); i++) {
			VOViaje viajeActual= ordenados.getElement(i);
			if(blockIdActual!=viajeActual.getBlockId()){
				blockIdActual=viajeActual.getBlockId();
				int idRepetidos=blockIdRepetidos(blockIdActual, ordenados);
				int[] tripsAsociados=tripIdAsociados(blockIdActual, ordenados);
				int routeId=viajeActual.getRouteId();

				Resumen nuevo= new Resumen(blockIdActual, routeId, idRepetidos, tripsAsociados);
				resumenes.add(nuevo);
			}
		}
		
		return resumenes;


	}


	public class Resumen{
		private int block_id;
		private int route_id;
		private int total_block_id;
		private int[] trip_id;

		public Resumen(int pBlock, int pRoute, int pTotal, int[] pTrips){
			block_id=pBlock ;
			route_id=pRoute;
			total_block_id=pTotal;
			trip_id=pTrips;
		}

		public int getBlockId(){
			return block_id;
		}

		public int getRouteId(){
			return route_id;
		}

		public int getTotalBlockId(){
			return total_block_id;
		}

		public int[] getTrips(){
			return trip_id;
		}
	}




}
