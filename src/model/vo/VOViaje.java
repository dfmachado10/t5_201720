package model.vo;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;
import model.data_structures.IQueue;
import model.data_structures.LinkedList;
import model.data_structures.Queue;

public class VOViaje 
{


	private String trip_headsign,trip_short_name,shape_id;
	
	private int service_id,direction_id,block_id,trip_id,route_id;

	private boolean wheelchair_accessible,bikes_allowed;

	public VOViaje(int pRouteId, int pTripId, int pServiceId, String pTripHead, String pShort, int pDirection,int pBLock,String pShape, boolean pWheelchair, boolean pBikes )
	{
		route_id=pRouteId;
		trip_id=pTripId;
		service_id=pServiceId;
		trip_headsign=pTripHead;
		trip_short_name=pShort;
		block_id=pBLock;
		shape_id=pShape;
		direction_id=pDirection;
		wheelchair_accessible=pWheelchair;
		bikes_allowed=pBikes;

	}

	public int getRouteId(){
		return route_id;
	}

	public int getIdViaje()
	{
		return trip_id;
	}

	public int getServiceId(){
		return service_id;
	}

	public String getHeadsing(){
		return trip_headsign;
	}

	public String getShortName(){
		return trip_short_name;
	}

	public int getDirectionId(){
		return direction_id;
	}

	public int getBlockId(){
		return block_id;
	}

	public String getShapeId(){
		return shape_id;
	}

	public boolean getWheelChairA(){
		return wheelchair_accessible;
	}

	public boolean getBikeA(){
		return bikes_allowed;
	}

}
