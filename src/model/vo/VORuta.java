package model.vo;

import java.io.BufferedReader;
import java.io.FileReader;

import model.data_structures.IList;
import model.data_structures.LinkedList;

/**
 * Representation of a route object
 */
public class VORuta {

	private String id;
	private String agency;
	private int short_name;
	private String long_name;
	private String desc;
	private int type;
	private String url;
	private String color;
	private String text_color;


	public VORuta(String pId, String pAgency, int pShort, String pLong, String pDesc, int pType, String pUrl, String pColor, String pText )
	{
		id=pId;
		agency=pAgency;
		short_name=pShort;
		long_name=pLong;
		desc=pDesc;
		type=pType;
		url=pUrl;
		color=pColor;
		text_color=pText;
	}

	/**
	 * @return id - Route's id number
	 */
	public String getId() {
		// TODO Auto-generated method stub
		return id;
	}

	/**
	 * @return name - route name
	 */
	public int getShortName() {
		// TODO Auto-generated method stub
		return short_name;
	}

	public String getLongName() {
		// TODO Auto-generated method stub
		return long_name;
	}

	public String getAgency(){
		return agency;
	}

	public String getDesc() {
		// TODO Auto-generated method stub
		return desc;
	}

	public int getType() {
		// TODO Auto-generated method stub
		return type;
	}

	public String getUrl() {
		// TODO Auto-generated method stub
		return url;
	}

	public String getColor() {
		// TODO Auto-generated method stub
		return color;
	}

	public String getTextColor() {
		// TODO Auto-generated method stub
		return text_color;
	}
	
	
}
