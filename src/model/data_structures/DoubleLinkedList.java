package model.data_structures;

import java.util.ArrayList;
import java.util.Iterator;


public class DoubleLinkedList<T> implements IList<T>
{
	private Node<T> primera = null;
	private Node<T> ultima = null;
	private Node<T> actualL = primera;
	private long tamahno = 0;
	
	public DoubleLinkedList(){
	}
	
	public T firstOne(){
		return primera != null? primera.elemento: null;
	}
	
	public T lastOne(){
		return ultima != null? ultima.elemento: null;
	}
	
	//DONE
	public void set(int pos, T pElemento){
		Node<T> actual = primera;
		if(actual == null || pos > tamahno || pos < 0) throw new IndexOutOfBoundsException();
		if(pos == tamahno-1) ultima.elemento = pElemento;
		else
		{
			for (int i = 0; i != pos; i++, actual = actual.siguiente);
			actual.elemento = pElemento;
		}
	}
	
	//DONE
	public int IndexOf(T elemento){
		int  resp = -1;
		if(elemento == null || primera == null) return resp;
		else
		{
			Node<T> actual = primera;
			for (resp = 0; resp < tamahno; resp++, actual = actual.siguiente) 
				if(actual.elemento == elemento) return resp;
		}
		return resp;
	}
	
	//DONE
	public boolean isEmpty(){
		return primera == null? true: false;
	}
	
	//DONE
	public boolean contains(T elemento) throws Exception{
		if(primera == null) return false;
		else
		{ 
			Node<T> actual = primera;
			for (int i = 0; i < tamahno; i++, actual = actual.siguiente) 
				if(actual.elemento == elemento) return true;
			return false;
		}
	}
	
	//DONE
	public boolean contains(T[] elementos){
		if(primera == null) return false;
		else
		{
			int correctos = 0;
			Node<T> actual = primera;
			for (int i = 0; i < elementos.length; i++) 
			{
				boolean encontrado = false;
				for (int j = 0; j < tamahno && !encontrado; j++, actual = actual.siguiente)
					if(actual.elemento == elementos[i]) correctos ++; encontrado = true;
			}
			if(correctos == elementos.length) return true;
			return false;
		}
	}

	public void next() {
	// TODO Auto-generated method stub
		if(actualL.siguiente != null) actualL = actualL.siguiente;
	}

	public void previous() {
	// TODO Auto-generated method stub
		if(actualL.anterior != null) actualL = actualL.anterior;
	}
	
	public T getCurrentElement() {
	// TODO Auto-generated method stub
		return actualL != null? actualL.elemento: null;
	}
	
	public Iterator<T> iterator() {
	// TODO Auto-generated method stub
		return new MiIterator<T>(primera);
	}

	public T getElement(int k) {
	// TODO Auto-generated method stub
		Node<T> actual = primera;
		if(actual == null || k > tamahno || k < 0) throw new IndexOutOfBoundsException();
		for (int i = 0; i != k; i++, actual = actual.siguiente);
		return actual.elemento;
	}

	public long getSize() {
	// TODO Auto-generated method stub
		return tamahno;
	}
	
	public T delete(T object) {
	// TODO Auto-generated method stub
		T eliminado = null;
		boolean eliminar = false;
		Node<T> actual = primera;
		if(actual == null) throw new IndexOutOfBoundsException();
		for(int i = 0; i < tamahno && !eliminar; i++, actual = actual.siguiente){
			if(actual.elemento == object){
				eliminado = actual.elemento;
				if(actual.siguiente == null){
					primera = ultima = actualL = null; tamahno--;
				}
				else{
					if(actual == primera){
						primera = actual.siguiente; primera.anterior = actual.siguiente = null; tamahno--;
					}
					else if(actual == ultima){
						ultima = actual.anterior; ultima.siguiente = actual.anterior = null; tamahno--;
					}
					else{
						actual.siguiente.anterior = actual.anterior; actual.anterior.siguiente = actual.siguiente;
						tamahno--;
					}
				}
			}
		}
		return eliminado;
	}

	public T deleteAtK(long k) {
	// TODO Auto-generated method stub
		T eliminado = null;
		Node<T> actual = primera;
		if(actual == null || k > tamahno || k < 0) throw new IndexOutOfBoundsException();
		if(k == 0) {
			eliminado = primera.elemento;
			if(primera.siguiente != null){
				primera = actual.siguiente; 
				primera.anterior = null; tamahno--;
				if(actualL.elemento == eliminado) next();
			}
			else{
				primera = ultima = actualL = null; tamahno--;
			}
		}
		else if(k == tamahno-1) {
			eliminado = ultima.elemento; ultima = ultima.anterior;
			ultima.siguiente = null; tamahno--;
			if(actualL.elemento == eliminado) next();
		}
		else{
			for(int i = 0; i != (k-1); i++ , actual = actual.siguiente);
			eliminado = actual.siguiente.elemento;
			actual.siguiente = actual.siguiente.siguiente; 
			actual.siguiente.anterior = actual; tamahno--;
			if(actualL.elemento == eliminado) next();
		}
		return eliminado;
	}
	
	public boolean addAtEnd(T object) {
		boolean a = false;
		Node<T> actual = ultima;
		ultima = new Node<T>(object, null, actual);
		if(actual == null){
			actualL = primera = ultima = new Node<T>(object, null, null); tamahno++; a = true;
		}
		else{
			actual.siguiente = ultima; tamahno++; a = true;
		}
		return a;
	}

	public boolean addAtK(T object, int K) {
		boolean a = false;
		Node<T> actual = primera;
		if(K > tamahno || K < 0) return false;
		if(K == 0){
			a = add(object);
		}
		else if(K == tamahno){
			a = addAtEnd(object);
		}
		else
		{
			for(int i = 0; i != K-1; i++ , actual = actual.siguiente);
			Node<T> x = new Node<T>(object, actual.siguiente, actual);
			actual.siguiente.anterior = x;
			actual.siguiente = x;
			tamahno++; a = true;
		}
		return a;
	}
	
	public boolean add(T object) {
		boolean a = false;
		Node<T> actual = primera;
		primera = new Node<T>(object, actual, null);
		if(actual != null){
			actual.anterior = primera; tamahno++; a=true;
		}
		else{
			actualL = ultima = primera; tamahno++; a=true;
		}
		return a;
	}

	//FIXME
	public static class Node<T>
	{
	T elemento;
	Node<T> anterior;
	Node<T> siguiente;
		public Node(T pElemento, Node<T> pSiguiente, Node<T> pAnterior)
		{
			elemento = pElemento;
			siguiente = pSiguiente;
			anterior = pAnterior;
		}
	}
	
	public class MiIterator<T> implements Iterator<T>
	{
		Node<T> actual;
		public MiIterator(Node<T> primera){
			actual = primera;
		}

		public boolean hasNext() {
			// TODO Auto-generated method stub
			return actual != null;
		}

		public T next() {
			// TODO Auto-generated method stub
			T elemento = actual.elemento;
			actual = actual.siguiente;
			return elemento;
		}
	}
}
