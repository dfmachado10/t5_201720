package model.data_structures;

import java.util.Iterator;

public interface IQueue<E> {
	
	public void enqueue(E item);
	
	public E dequeue();
	
	public Iterator<E> iterator();

}
