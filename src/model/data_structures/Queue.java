package model.data_structures;

import java.util.Iterator;


public class Queue<T> implements IQueue<T> {
	
	private LinkedList<T> array;
	
	
	public Queue(){
		array = new LinkedList<T>();
	}

	public void enqueue(T item) {
		// TODO Auto-generated method stub
		array.addAtEnd(item);
	}

	public T dequeue() {
		// TODO Auto-generated method stub
		return array.deleteAtK(0);
	}
	
	public boolean isEmpty(){
		return array.isEmpty();
	}
	
	public int size(){
		return (int) array.getSize();
	}
	
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return array.iterator();
	}
	
	

}
