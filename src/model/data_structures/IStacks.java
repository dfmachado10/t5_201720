package model.data_structures;

public interface IStacks<E> {

	
	public void push (E item);
	
	public E pop();
}
