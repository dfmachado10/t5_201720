package model.data_structures;

public class Stacks<T> implements IStacks<T> {
	
	private LinkedList<T> array;
	
	public Stacks(){
		array = new LinkedList<T>();
	}
	
	public void push(T item) {
		// TODO Auto-generated method stub
		array.addAtEnd(item);
	}

	public T pop() {
		// TODO Auto-generated method stub
		return array.deleteAtK((int)array.getSize()-1);
	}
	
	public boolean isEmpty(){
		return array.isEmpty();
	}
	
	public int size(){
		return (int) array.getSize();
	}
}
